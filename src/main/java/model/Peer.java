package model;

import java.util.List;

public class Peer {
    private String peerId;
    private String ipAddress;
    private int port;
    private String filesDirectory;
    private List<String> sharedFiles;

    public Peer(peer.Peer peer) {
        peerId = peer.getPeerId();
        ipAddress = peer.getIpAddress();
        port = peer.getPort();
        filesDirectory = peer.getFilesDirectory();
        sharedFiles = peer.getSharedFiles();
    }

    public String getPeerId() {
        return peerId;
    }

    public void setPeerId(String peerId) {
        this.peerId = peerId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFilesDirectory() {
        return filesDirectory;
    }

    public void setFilesDirectory(String filesDirectory) {
        this.filesDirectory = filesDirectory;
    }

    public List<String> getSharedFiles() {
        return sharedFiles;
    }

    public void setSharedFiles(List<String> sharedFiles) {
        this.sharedFiles = sharedFiles;
    }
}
