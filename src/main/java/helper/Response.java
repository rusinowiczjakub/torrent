package helper;

import model.Peer;

import java.util.List;

public class Response {

	public String status;
	public List<Peer> peersList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Peer> getPeersList() {
		return peersList;
	}

	public void setPeersList(List<Peer> peersList) {
		this.peersList = peersList;
	}
}
