package helper;

import model.Peer;

import java.io.Serializable;

public class Notification implements Serializable {
    private Peer model;
    private String searchFile;
    private int action;

    public Peer getModel() {
        return model;
    }

    public void setModel(Peer model) {
        this.model = model;
    }

    public String getSearchFile() {
        return searchFile;
    }

    public void setSearchFile(String searchFile) {
        this.searchFile = searchFile;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
