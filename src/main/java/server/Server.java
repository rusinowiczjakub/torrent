package server;

import helper.Notification;
import helper.Response;
import model.Peer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Server extends Thread implements Runnable {


	public static int REGISTER_ACTION = 1;
	public static int SEARCH_ACTION = 2;
	public static final String SUCCESS = "success";

	private static Map<String, Peer> peersMap = new HashMap<String, Peer>();
	private Socket s;


	public Server(Socket s) {
		this.s = s;
	}

	public static Map<String, Peer> getPeersMap() {
		return peersMap;
	}

	public static void setPeersMap(Map<String, Peer> peersMap) {
		Server.peersMap = peersMap;
	}

	public Socket getS() {
		return s;
	}

	public void setS(Socket s) {
		this.s = s;
	}

	public boolean registration(Peer peer) {

		//For calculating the time to register a peer
		final long startTime = System.currentTimeMillis();
		System.out.println("The peer id " + peer.getPeerId() + " is getting registered by the Server.");
		boolean registered = false;
		Peer tempPeer = peersMap.get(peer.getPeerId());

		//Peer is registering file for the first time.

		if (tempPeer == null) {

			peersMap.put(peer.getPeerId(), peer);
			registered = true;

		} else {

			tempPeer.getSharedFiles().addAll(peer.getSharedFiles());
			peersMap.put(peer.getPeerId(), tempPeer);
			registered = true;
		}

		final long endTime = System.currentTimeMillis();
		System.out.println("Server is done with registering peer id  " + peer.getPeerId() + " in " + (endTime - startTime) + " milliseconds");
		return registered;
	}

	/**********************************************************************************************************************************************************/

	// This method search for the file and return the list
	public List<Peer> searching(String fileName) {
		final long startTime = System.currentTimeMillis();
		System.out.println("Searching for the file " + fileName);
		List<Peer> listOfPeers = new ArrayList<Peer>();

		for (Map.Entry<String, Peer> entry : peersMap.entrySet()) {

			Peer currentPeerModel = entry.getValue();

			// Searching file name
			if (currentPeerModel.getSharedFiles().contains(fileName)) {

				listOfPeers.add(currentPeerModel);

			}

		}

		final long endTime = System.currentTimeMillis();
		System.out.println("Search completed for the file " + fileName + " in  " + (endTime - startTime) + " milliseconds");
		return listOfPeers;
	}


	/**********************************************************************************************************************************************************/

	@Override
	public void run() {

		boolean connected = true;

		do {
			try {
				ObjectInputStream inp = new ObjectInputStream(s.getInputStream());
				ObjectOutputStream oup = new ObjectOutputStream(s.getOutputStream());
				Notification notification = (Notification) inp.readObject();
				Response rfs = new Response();


				if (REGISTER_ACTION == notification.getAction()) {
					this.registration(notification.getModel());

				} else if (SEARCH_ACTION == notification.getAction()) {
					List<Peer> listOfPeers = this.searching(notification.getSearchFile());
					rfs.setPeersList(listOfPeers);
				}

				rfs.setStatus(SUCCESS);

				//Writing the content
				oup.writeObject(rfs);
				oup.flush();
				inp.close();
				connected = false;


			} catch (Exception ex) {
				ex.printStackTrace();
			}

		} while (connected);

	}

	/**********************************************************************************************************************************************************/

	public static void main(String args[]) throws Exception {
		//Creating a connection between server and the client
		ServerSocket ss = new ServerSocket(8899);


		while (true) {
			System.out.println("Server waiting to connect to the peer");
			//connecting
			Socket ps = ss.accept();
			new Server(ps).start();
		}


	}
}
