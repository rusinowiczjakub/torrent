package peer;

import helper.Notification;
import helper.Response;

import java.io.*;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public abstract class Peer extends Thread {

	public static int CONNECTING = 1;
	public static int SEARCH = 2;
	public static int RECEIVE = 3;
	public static int SEND = 4;
	public static int RECEIVE_FILE = 5;

	private Socket socket;
	private String peerId;
	private String ipAddress;
	private int port;
	private static String serverHostname;
	private String filesDirectory;
	private int serverPort;

	public Peer() {

	}

	public boolean searchFile(String fileName) throws Exception, ClassNotFoundException {
		Notification notification = new Notification();
		notification.setAction(SEARCH);
		notification.setSearchFile(fileName);

		Socket socket = new Socket(serverHostname, serverPort);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
		objectOutputStream.writeObject(notification);

		ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
		Response response = (Response) objectInputStream.readObject();

		if (response.getPeersList().size() > 0) {
			System.out.println("File " + fileName + " found, Peers number: " + response.getPeersList().size());

			download(notification.getSearchFile(), response.getPeersList().get(0));

			return true;
		}

		System.out.println("File not found");

		return false;
	}

	public void connect() throws IOException {

		Notification notification = new Notification();
		notification.setAction(CONNECTING);

		socket = new Socket(serverHostname, serverPort);

		System.out.println("Connecting peer: " + peerId);
		System.out.println("                          ");

		ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());

		model.Peer peer = new model.Peer(this);
		notification.setModel(peer);

		outputStream.writeObject(notification);
		outputStream.flush();

		ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

		System.out.println("-----------------------------------");
		System.out.println("PEER: " + notification.getModel().getPeerId() + "connected to server");
		System.out.println("-----------------------------------");


	}

	public List<String> getSharedFiles() {
		File directory = new File(filesDirectory);
		List<String> fileList = Arrays.asList(directory.list());

		return fileList;
	}

	public void download(String fileName, model.Peer providingPeer) {
		ObjectOutputStream objectOutputStream = null;
		ObjectInputStream objectInputStream = null;

		try {
			System.out.println("Downloading file " + fileName + " from: " + providingPeer.getPeerId());
			Notification notification = new Notification();
			notification.setAction(RECEIVE_FILE);
			notification.setSearchFile(fileName);
			Socket socket = new Socket(providingPeer.getIpAddress(), providingPeer.getPort());
			objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
			objectOutputStream.writeObject(notification);
			objectOutputStream.flush();

			objectInputStream = new ObjectInputStream(socket.getInputStream());
			String fileData = objectInputStream.readObject().toString();

			FileWriter fileWriter = new FileWriter(filesDirectory + "\\" + fileName, true);
			fileWriter.write(fileData);
			fileWriter.close();
			System.out.println("                                            ");
			System.out.println("Downloading " + fileName + " finished successfully");
		} catch (Exception exception) {

		}
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getPeerId() {
		return peerId;
	}

	public void setPeerId(String id) {
		this.peerId = id;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getFilesDirectory() {
		return filesDirectory;
	}

	public void setFilesDirectory(String filesDirectory) {
		this.filesDirectory = filesDirectory;
	}

	@Override
	public void run() {
		Notification notification = null;
		boolean connected = true;

		try {
			ServerSocket serverSocket = new ServerSocket(port);

			while (true) {
				System.out.println("             ");
				System.out.println("=============================================");
				System.out.println("PEER " + peerId + " running on port " + port);
				System.out.println("=============================================");
				Socket newPeer = serverSocket.accept();

				Class<?> clazz = Class.forName(this.getClass().getName());
				Constructor<?> ctor = clazz.getConstructor(String.class);

				((Peer) ctor.newInstance(new Object[]{newPeer})).run();
			}
		} catch (Exception exception) {

		}

		do {
			try {
				ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());

				notification = (Notification) objectInputStream.readObject();
				System.out.println(notification.getModel().getPeerId() + " requested file " + notification.getSearchFile());

				String fileData = new String(Files.readAllBytes(Paths.get(filesDirectory + "\\" + notification.getSearchFile())));
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
				objectOutputStream.writeObject(fileData);
				objectOutputStream.flush();

				connected = false;
			} catch (Exception exception) {

			}
		} while (connected);
	}

	public void performAction(String userInput) throws Exception {
		Scanner scanner;

		switch (userInput) {
			case "1":
				this.connect();

				break;

			case "2":
				System.out.println("                 ");
				System.out.println("Enter the file name to be searched");
				scanner = new Scanner(System.in);
				String fileName = scanner.nextLine();
				System.out.println("                 ");
				System.out.println("Peer " + peerId + " requested file" + fileName);
				this.searchFile(fileName);

				break;

			case "4":
				System.out.println("Exiting");
				System.exit(0);

				break;

			default:
				System.out.println("Wrong input");

				break;
		}
	}

	public void main(String args[]) {
		Scanner scanner;

		try {
			Class<?> clazz = Class.forName(this.getClass().getName());
			Constructor<?> ctor = clazz.getConstructor(String.class);
			((Peer) ctor.newInstance(new Object[]{})).start();

			String localhost = InetAddress.getLocalHost().getHostName();
			System.out.println("Enter Server host name (default: " + localhost + "):");

			String userInput = new Scanner(System.in).nextLine();
			if (userInput.length() < 1) {
				serverHostname = localhost;
			} else {
				serverHostname = userInput;
			}

			System.out.println("Give port to run this PEER");

			int userPort = Integer.valueOf(new Scanner(System.in).nextLine());

			port = userPort;

			this.start();

			scanner = new Scanner(System.in);

			String userAction = scanner.nextLine();

			while (true) {
				System.out.println("*******************************************************");
				System.out.println("Choose action");
				System.out.println("   ");
				System.out.println("Enter 1 to CONNECT peer 1");
				System.out.println("Enter 2 to SEARCH for a file");
				System.out.println("Enter 3 for 1000 sequential request");
				System.out.println("Enter 4 to exit");
				System.out.println("           ");
				System.out.println("*******************************************************");

				performAction(userAction);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
